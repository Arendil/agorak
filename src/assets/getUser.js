export default function getUser(){ 
	let user = firebase.auth().currentUser;
	let userData = "";
	let isLogged = false;
	if (user != null) {
		userData = {
			email 		  : user.email,
			emailVerified : user.emailVerified,
			uid 		  : user.uid
		}
		isLogged = true
	}else{
		isLogged = false;
		userData = "";
	}
	return userData;
}